#!/usr/bin/python3

# Python3 基础语法

## 默认 以 UTF-8 编码
# -*- coding: utf8 -*-

## python保留字
import keyword
print(keyword.kwlist)
'''
['False', 'None', 'True', 'and', 'as', 'assert', 'async', 'await', 'break', 'class', 'continue', 'def', 'del',
 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda', 'nonlocal',
  'not', 'or', 'pass', 'raise', 'return', 'try', 'while', 'with', 'yield']
'''

## 注释
'''
这是一种注释
'''
"""
这也是一种注释
"""

## python最具特色的就是使用缩进来表示代码块，不需要使用大括号 {}
if True:
    print ("True")
else:
    print ("False")
### 缩进不一致，会导致运行错误

## 多行语句: Python 通常是一行写完一条语句，但如果语句很长，我们可以使用反斜杠(\)来实现多行语句
num_1 = 1
num_2 = 2
num_3 = 3
total = num_1 + \
    num_2 + \
    num_3
print(total)
### 在 [], {}, 或 () 中的多行语句，不需要使用反斜杠(\)，例如：
total = ['1', '2', '3',
         '4', '5']
print(total)

## 数字(Number)类型: python中数字有四种类型：整数、布尔型、浮点数和复数。
### int (整数), 如 1, 只有一种整数类型 int，表示为长整型，没有 python2 中的 Long。
num_int = 123
### bool (布尔), 如 True
'''
在 Python2 中是没有布尔型的，它用数字 0 表示 False，用 1 表示 True。
到 Python3 中，把 True 和 False 定义成关键字了，但它们的值还是 1 和 0，它们可以和数字相加。
'''
val_boolean = True
### float (浮点数), 如 1.23、3E-2
num_float = 1.23
### complex (复数), 如 1 + 2j、 1.1 + 2.2j
'''
复数由实数部分和虚数部分构成，可以用a + bj,或者complex(a,b)表示， 
复数的实部a和虚部b都是浮点型
'''
num_complex = 1 + 2j
print(num_int + num_float + num_complex)
print(num_int + num_float + num_complex + val_boolean)

## 字符串(String): 【不能改变】；没有单独的字符类型，一个字符就是长度为 1 的字符串
### 单引号和双引号使用完全相同
str_1 = '字符串'
str_2 = "字符串"
print(str_1, str_2)
### 使用三引号('''或""")可以指定一个多行字符串
str_3 = '''【段落123
456】
'''
print(str_3)
### 转义符 反斜杠'\',使用r可以让反斜杠不发生转义。。 如 r"this is a line with \n" 则\n会显示，并不是换行
str_4 = '123\s'
str_4_ = '123\\n456'
str_5 = r'123\n456'
print(str_4, str_4_, str_5)
### 按字面意义级联字符串，如"this " "is " "string"会被自动转换为this is string。
str_6 = "this " "is " "String"
print(str_6)
### 字符串可以用 + 运算符连接在一起，用 * 运算符重复。
str_7 = '123' + '456'
print(str_7, str_7 + '0000')
str_8 = '123' * 5
print(str_8)
### Python 中的字符串有两种索引方式，从左往右以 0 开始，从右往左以 -1 开始
### 字符串的截取的语法格式如下：变量[头下标:尾下标:步长]
str_9 = '1234567890'
print('------------------------------')
print(str_9)
print(str_9[0])     # 输出字符串第一个字符
print(str_9[0:-1])  # 输出第一个到倒数第二个的所有字符
print(str_9[2:5])   # 输出从第三个开始到第五个的字符
print(str_9[2:])    # 输出从第三个开始的后的所有字符
print(str_9[0:6:2]) # 135
### 格式化输出
print ("我叫 %s 今年 %d 岁!" % ('小明', 10))

## 空行（空行也是程序代码的一部分。）
'''
函数之间或类的方法之间用空行分隔，表示一段新的代码的开始。
类和函数入口之间也用一行空行分隔，以突出函数入口的开始。
空行与代码缩进不同，空行并不是Python语法的一部分；书写时不插入空行，Python解释器运行也不会出错。
空行的作用在于分隔两段不同功能或含义的代码，便于日后代码的维护或重构。
'''

## 等待用户输入
#input("\n\n按下 enter 键后退出。")

## 同一行显示多条语句：Python可以在同一行中使用多条语句，语句之间使用分号(;)分割。
print('------------------------------')
import sys; x = 'runoob'; sys.stdout.write(x + '\n')

## 多个语句构成代码组：缩进相同的一组语句构成一个代码块，我们称之代码组
'''
像if、while、def和class这样的复合语句，首行以关键字开始，以冒号( : )结束，该行之后的一行或多行代码构成代码组。
我们将首行及后面的代码组称为一个子句(clause)。
'''
xx = 123
if xx > 100 :
    print("大于100")
elif xx > 120 :
    print("大于120")
else :
    print("其他")

## print输出：默认输出是换行的，如果要实现不换行需要在变量末尾加上 end=""：
print('------------------------------')
print("123")
print("123", end = "")
print('------------------------------')

## import 与 from...import
'''
在 python 用 import 或者 from...import 来导入相应的模块
将整个模块(somemodule)导入，格式为： import somemodule
从某个模块中导入某个函数,格式为： from somemodule import somefunction
从某个模块中导入多个函数,格式为： from somemodule import firstfunc, secondfunc, thirdfunc
将某个模块中的全部函数导入，格式为： from somemodule import *
'''
### 导入 sys 模块
import sys
print('================Python import mode==========================')
print ('命令行参数为:')
for i in sys.argv:
    print (i)
print ('\n python 路径为',sys.path)
### 导入 sys 模块的 argv,path 成员
from sys import path  #  导入特定的成员
print('================python from import===================================')
print('path:', path) # 因为已经导入path成员，所以此处引用时不需要加sys.path

## Python逻辑运算符
'''
x and y	布尔"与" - 如果 x 为 False，x and y 返回 False，否则它返回 y 的计算值。
x or y	布尔"或" - 如果 x 是 True，它返回 x 的值，否则它返回 y 的计算值。
not x	布尔"非" - 如果 x 为 True，返回 False 。如果 x 为 False，它返回 True。
'''
x, y = 10, 20
print('------------------------------')
print(x and y)       # 20
print(x or y)        # 10
print(not(x))        # False
print(not(x and y))  # False
print(not(x or y))   # False
if ( x and y ):
    print ("1 - 变量 x 和 y 都为 true")
else:
    print ("1 - 变量 x 和 y 至少有一个不为 true")
if ( x or y ):
    print ("2 - 变量 x 和 y 至少有一个为 true")
else:
    print ("2 - 变量 x 和 y 都不为 true")
### 修改变量 x 的值
x = 0
if ( x and y ):
    print ("3 - 变量 x 和 y 都为 true")
else:
    print ("3 - 变量 x 和 y 至少有一个不为 true")
if ( x or y ):
    print ("4 - 变量 x 和 y 至少有一个为 true")
else:
    print ("4 - 变量 x 和 y 都不为 true")
if not( x and y ):
    print ("5 - 变量 x 和 y 至少有一个为 false")
else:
    print ("5 - 变量 x 和 y 都为 true")

## Python成员运算符
'''
in	如果在指定的序列中找到值返回 True，否则返回 False。
not in	如果在指定的序列中没有找到值返回 True，否则返回 False。
'''
a = 10
b = 20
list = [1, 2, 3, 4, 5 ];
print('------------------------------')
if (a in list):
    print("1 - 变量 a 在给定的列表中 list 中")
else:
    print("1 - 变量 a 不在给定的列表中 list 中")
if (b not in list):
    print("2 - 变量 b 不在给定的列表中 list 中")
else:
    print("2 - 变量 b 在给定的列表中 list 中")
# 修改变量 a 的值
a = 2
if (a in list):
    print("3 - 变量 a 在给定的列表中 list 中")
else:
    print("3 - 变量 a 不在给定的列表中 list 中")

## Python身份运算符
'''
is	是判断两个标识符是不是引用自一个对象	x is y, 类似 id(x) == id(y) , 如果引用的是同一个对象则返回 True，否则返回 False
is not 是判断两个标识符是不是引用自不同对象	x is not y ， 类似 id(a) != id(b)。如果引用的不是同一个对象则返回结果 True，否则返回 False。
'''
### id() 函数用于获取对象内存地址。
print('------------------------------')
a = 20
b = 20
if (a is b): # True
    print("1 - a 和 b 有相同的标识")
else:
    print("1 - a 和 b 没有相同的标识")
if (id(a) == id(b)):
    print("2 - a 和 b 有相同的标识")
else:
    print("2 - a 和 b 没有相同的标识")
# 修改变量 b 的值
b = 30
if (a is b): # False
    print("3 - a 和 b 有相同的标识")
else:
    print("3 - a 和 b 没有相同的标识")
if (a is not b):
    print("4 - a 和 b 没有相同的标识")
else:
    print("4 - a 和 b 有相同的标识")
'''
is 与 == 区别：
is 用于判断两个变量引用对象是否为同一个， == 用于判断引用变量的值是否相等。
'''
a = [1, 2, 3]
b = a
print(b is a)   # true
print(b == a)   # true
b = a[:]
print(a, b)
print(a, b)
print(b is a)   # False
print(b == a)   # True

## Python运算符优先级
'''
最高到最低优先级的所有运算符：
**	                            指数 (最高优先级)
~ + -	                        按位翻转, 一元加号和减号 (最后两个的方法名为 +@ 和 -@)
* / % //	                    乘，除，取模和取整除
+ -	                            加法减法
>> <<	                        右移，左移运算符
&	                            位 'AND'
^ |	                            位运算符
<= < > >=	                    比较运算符
<> == !=	                    等于运算符
= %= /= //= -= += *= **=	    赋值运算符
is is not	                    身份运算符
in not in	                    成员运算符
and or not	                    逻辑运算符
'''
a = 20
b = 10
c = 15
d = 5
e = 0
e = (a + b) * c / d       #( 30 * 15 ) / 5
print ("(a + b) * c / d 运算结果为：",  e)
e = ((a + b) * c) / d     # (30 * 15 ) / 5
print ("((a + b) * c) / d 运算结果为：",  e)
e = (a + b) * (c / d);    # (30) * (15/5)
print ("(a + b) * (c / d) 运算结果为：",  e)
e = a + (b * c) / d;      #  20 + (150/5)
print ("a + (b * c) / d 运算结果为：",  e)


## None 与 ""(即空字符)的区别
'''
None 表示该值是一个空对象，空值是Python里一个特殊的值，用None表示。None不能理解为0，因为0是有意义的，而None是一个特殊的空值。
可以将None赋值给任何变量，也可以将任何变量赋值给一个None值得对象
'''
### 判断的时候都是 False
if None:
    print("Hello")
if "":
    print("空字符串")
if "xx":
    print("非空字符串")
### 是不同的一种数据类型
print(type(None), type("")) # <class 'NoneType'> <class 'str'>
### 属性不同
'''
使用dir()函数返回参数的属性、方法列表。如果参数包含方法dir()，该方法将被调用。如果参数不包含dir()，该方法将最大限度地收集参数信息。
'''
print(dir(None))
print(dir(""))