# Python3 数据结构
## 列表：列表可以修改，而字符串和元组不能。
'''
insert, remove 或 sort 等修改列表的方法没有返回值
'''
a = [66.25, 333, 333, 1, 1234.5]
print(a)
print(a.count(333), a.count(66.25), a.count('x'))  # 返回 x 在列表中出现的次数。
a.insert(2, -1)
print(a)
a.append(333)  # 把一个元素添加到列表的结尾，相当于 a[len(a):] = [x]。
print(a)
print(a.index(333))
a.remove(333)  # 删除列表中值为 x 的第一个元素。如果没有这样的元素，就会返回一个错误。
print(a)
a.reverse()
print(a)
a.sort()
print(a)
b = a.copy()   # 返回列表的浅复制，等于a[:]，浅复制后的列表的修改不会影响原列表。
print(a, b)
b.pop(-1)      # 从列表的指定位置移除元素，并将其返回,-1代表最后一个元素。
b.pop()        # 返回最后一个元素。元素随即从列表中被移除
print(a, b)
a.clear()      # 移除列表中的所有项，等于del a[:]。
print(a, b)
print("------------------------")

## 将列表当做堆栈使用
'''
列表方法使得列表可以很方便的作为一个堆栈来使用，堆栈作为特定的数据结构，最先进入的元素最后一个被释放（后进先出）。
用 append() 方法可以把一个元素添加到堆栈顶。
用不指定索引的 pop() 方法可以把一个元素从堆栈顶释放出来。例如：
'''
stack = [3, 4, 5]
print(stack)
stack.append(6)
stack.append(7)
print(stack)
print(stack.pop(), stack)
print(stack.pop(), stack)
print(stack.pop(), stack)
print("------------------------")

## 将列表当作队列使用【效率不高】
'''
也可以把列表当做队列用，只是在队列里第一加入的元素，第一个取出来；但是拿列表用作这样的目的【效率不高】。
在列表的最后添加或者弹出元素速度快，然而在列表里插入或者从头部弹出速度却不快（因为所有其他的元素都得一个一个地移动）。
'''
### 队列可以使用 collections 包中的 deque。
from collections import deque

queue = deque(["Eric", "John", "Michael"])
print(queue)
queue.append("Terry")          # Terry arrives
queue.append("Graham")         # Graham arrives
print(queue)
print(queue.popleft(), queue)  # The first to arrive now leaves
print(queue.popleft(), queue)  # The second to arrive now leaves
print(queue)                   # Remaining queue in order of arrival
print("------------------------")

## 列表推导式
'''提供了从序列创建列表的简单途径
通常应用程序将一些操作应用于某个序列的每个元素，用其获得的结果作为生成新列表的元素，或者根据确定的判定条件创建子序列。
每个列表推导式都在 for 之后跟一个表达式，然后有零到多个 for 或 if 子句。
返回结果是一个根据表达式从其后的 for 和 if 上下文环境中生成出来的列表。
如果希望表达式推导出一个元组，就必须使用括号。
'''
### 将列表中每个数值乘三，获得一个新的列表：
vec = [2, 4, 6]
print(vec, [3 * x for x in vec])
print([[x, x ** 2] for x in vec])
freshfruit = ['  banana', '  loganberry ', 'passion fruit  ']
print(freshfruit, [weapon.strip() for weapon in freshfruit])
### 还可以使用 if 子句作为过滤器
print([3 * x for x in vec if x > 3])
### 多个列表循环
vec1 = [2, 4, 6]
vec2 = [4, 3, -9]
print(vec1, vec2, [x * y for x in vec1 for y in vec2])
print([x + y for x in vec1 for y in vec2])
print([vec1[i] * vec2[i] for i in range(len(vec1))])
### 使用复杂表达式或嵌套函数
print([str(round(355/113, i)) for i in range(1, 8)])
print("------------------------")

## 嵌套列表解析：Python的列表还可以嵌套。
### 3X4的矩阵列表
matrix = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
    ]
print(matrix)
### 将3X4的矩阵列表转换为4X3列表：
print([[x[i] for x in matrix] for i in range(len(matrix[0]))])
### 或者这样实现
transposed = []
for i in range(len(matrix[0])):
    transposed.append([x[i] for x in matrix])
print(transposed)
### 或者更笨的一点的实现方式：
transposed = []
for i in range(len(matrix[0])):
    transposed_row = []
    for mRow in matrix:
        transposed_row.append(mRow[i])
    transposed.append(transposed_row)
print(transposed)
transposed = []
for i in range(4):
    transposed_row = []
    for j in range(3):
        transposed_row.append(matrix[j][i])
    transposed.append(transposed_row)
print(transposed)
print("------------------------")

## del 语句
'''
使用 del 语句可以从一个列表中依索引而不是值来删除一个元素。
这与使用 pop() 返回一个值不同。可以用 del 语句从列表中删除一个，切割，或清空整个列表
'''
a = [-1, 1, 66.25, 333, 333, 1234.5]
print(a)
del a[0]
print(a)
del a[2:4]
print(a)
del a[:]
print(a)
### 也可以用 del 删除实体变量：
del a
# print(a) # NameError: name 'a' is not defined
print("------------------------")

## 元组和序列：元组由若干逗号分隔的值组成
'''
元组在输出时总是有括号的，以便于正确表达嵌套结构。
在输入时可能有或没有括号， 不过括号通常是必须的（如果元组是更大的表达式的一部分）
'''
t = 12345, 54321, 'hello!'
print(t[0], t)
### 元组可以嵌套
u = t, (1, 2, 3, 4, 5)
print(u)
print("------------------------")

## 集合
'''
集合是一个无序不重复元素的集。基本功能包括关系测试和消除重复元素。
可以用大括号 {} 创建集合。
注意：如果要创建一个空集合，你必须用 set() 而不是 {} ；后者创建一个空的字典。
'''
basket = set()
print(basket)
basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
print(basket)                      # 删除重复的
print('orange' in basket)          # 检测成员
print('crabgrass' in basket)
a = set('abracadabra')             # 只接受一个参数
b = set('alacazam')
print(a, b)                        # a 中唯一的字母
print(a - b)                       # 在 a 中的字母，但不在 b 中
print(a | b)                       # 在 a 或 b 中的字母
print(a & b)                       # 在 a 和 b 中都有的字母
print(a ^ b)                       # 在 a 或 b 中的字母，但不同时在 a 和 b 中
### 集合也支持推导式：
print({x for x in 'abracadabra' if x not in 'abc'})
print("------------------------")

## 字典
'''
序列是以连续的整数为索引，与此不同的是，字典以关键字为索引，关键字可以是任意不可变类型，通常用字符串或数值。
理解字典的最佳方式是把它看做 无序的 键=>值 对集合。
在同一个字典之内，关键字必须是互不相同。
一对大括号创建一个空的字典：{}。
'''
tel = {}
print(tel)
tel = {'jack': 4098, 'sape': 4139}
print(tel)
tel['guido'] = 4127
print(tel)
print(tel['jack'])
del tel['sape']
print(tel)
tel['irv'] = 4127
print(tel)
print(list(tel.keys()))
print(sorted(tel.keys()))
print('guido' in tel)     # True
print('jack' not in tel)  # False
### 构造函数 dict() 直接从键值对元组列表中构建字典。
print(dict([('sape', 4139), ('guido', 4127), ('jack', 4098)]))
### 字典推导可以用来创建任意键和值的表达式词典。
print({x: x ** 2 for x in (2, 4, 6)})
### 如果关键字只是简单的字符串，使用关键字参数指定键值对有时候更方便：
print(dict(sape=4139, guido=4127, jack=4098))
print("------------------------")

## 遍历技巧
### 在字典中遍历时，关键字和对应的值可以使用 items() 方法同时解读出来
knights = {'gallahad': 'the pure', 'robin': 'the brave'}
for k, v in knights.items():
    print(k, ":", v, end=", ")
print()
### 枚举：在序列中遍历时，索引位置和对应值可以使用 enumerate() 函数同时得到：
for index, v in enumerate(knights):
    print(index, v, end=", ")
print()
### 也可以用于 列表的遍历
for index, v in enumerate([-1, 1, 66.25, 333, 333, 1234.5]):
    print(index, v, end=", ")
print()
### 同时遍历两个或更多的序列，可以使用 zip() 组合：
questions = ['name', 'quest', 'favorite color']
answers = ['lancelot', 'the holy grail', 'blue']
for q, a in zip(questions, answers):
    print('What is your {0}?  It is {1}.'.format(q, a))
### 反向遍历一个序列，首先指定这个序列，然后调用 reversed() 函数：
for i in reversed(range(1, 10, 2)):
    print(i, end=", ")
print()
### 按顺序遍历一个序列，使用 sorted() 函数返回一个已排序的序列，并不修改原值：
basket = ['apple', 'orange', 'apple', 'pear', 'orange', 'banana']
for f in sorted(set(basket)):
    print(f, end=", ")
print()
print(basket) # 并不会修改原序列
print("------------------------")