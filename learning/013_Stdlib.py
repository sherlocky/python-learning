# Python3 标准库概览
'''
建议使用 "import xx" 风格而非 "from xx import *"，这样可以保证不会覆盖重名的内置函数。
'''
## 操作系统接口
'''os模块提供了不少与操作系统相关联的函数'''
import os
print(os.getcwd()) # 返回当前的工作目录
#os.chdir('../')   # 修改当前的工作目录
#os.system('dir')   # 执行系统命令 dir
'''针对日常的文件和目录管理任务，:mod:shutil 模块提供了一个易于使用的高级接口'''
import shutil
#shutil.copyfile('data.db', 'archive.db')
#shutil.move('/build/executables', 'installdir')

## 文件通配符
'''glob模块提供了一个函数用于从目录通配符搜索中生成文件列表:'''
import glob
print(glob.glob('*.py'))

## 命令行参数
'''通用工具脚本经常调用命令行参数。这些命令行参数以链表形式存储于 sys 模块的 argv 变量。'''
import sys
print(sys.argv)

## 错误输出重定向和程序终止
'''
sys 还有 stdin，stdout 和 stderr 属性，即使在 stdout 被重定向时，后者也可以用于显示警告和错误信息
大多脚本的定向终止都使用 "sys.exit()"。
'''
sys.stderr.write('Warning, log file not found starting a new one\n')

## 字符串正则匹配
'''re模块为高级字符串处理提供了正则表达式工具。对于复杂的匹配和处理，正则表达式提供了简洁、优化的解决方案'''
import re
print(re.findall(r'\bf[a-z]*', 'which foot or hand fell fastest')) # 找到所有匹配的子串，返回一个列表，找不到，则返回空列表
print(re.sub(r'(\b[a-z]+) \1', r'\1', 'cat in the the hat')) # 替换字符串中的匹配项
'''如果只需要简单的功能，应该首先考虑字符串方法，因为它们非常简单，易于阅读和调试:'''
print('tea for too'.replace('too', 'two'))

## 数学
'''math模块为浮点运算提供了对底层C函数库的访问'''
import math
print(math.pi)
print(math.cos(math.pi / 4))
print(math.log(1024, 2))
'''random提供了生成随机数的工具'''
import random
print(random.choice(['apple', 'pear', 'banana'])) # 随机选择一个输出
print(random.sample(range(100), 10))   # 在0~99范围内，随机生成10个
print(random.random()) # 随机生成 float 数
print(random.randrange(6)) # 随机生成整数 0 ~ 5

## 访问 互联网
'''
有几个模块用于访问互联网以及处理网络通信协议。
其中最简单的两个是用于处理从 urls 接收的数据的 urllib.request 
以及用于发送电子邮件的 smtplib.
'''
import urllib.request
for line in urllib.request.urlopen('http://tool.ckd.cc/worldclock.php'):
    line = line.decode('utf-8')  # 解码二进制数据到 文本
    if '中国' in line or '新加坡' in line:  # 查找中国和新加坡时间
        print(line)
'''smtp 邮件发送，需要本地有一个在运行的邮件服务器'''
#import smtplib
#server = smtplib.SMTP('localhost')
#server.sendmail('soothsayer@example.org', 'jcaesar@example.org')
#server.quit()

## 日期和时间
'''
datetime模块为日期和时间处理同时提供了简单和复杂的方法。
支持日期和时间算法的同时，实现的重点放在更有效的处理和格式化输出，该模块还支持时区处理。
'''
import datetime
now = datetime.date.today()
print(now) # 2019-11-28 格式
print(datetime.date(2003, 12, 2)) # 初始化一个date
print(now.strftime("%Y-%m-%d %H:%M:%S"))
print(now.strftime("%y-%m-%d. %d %b %Y is a %A on the %d day of %B.")) # 格式化
'''支持日历算术'''
birthday = datetime.date(1964, 7, 31)
age = now - birthday
print("相差 %d 天" % (age.days))

## 数据压缩
'''zlib，gzip，bz2，zipfile，以及 tarfile，支持通用的数据打包和压缩格式。'''
import zlib
s = b'witch which has which witches wrist watch' # b'' 表示 bytes字节符，打印以b开头
print(len(s))
t = zlib.compress(s)
print(len(t))
print(zlib.decompress(t))
print(zlib.crc32(s))

## 性能度量: Python 提供了一个性能度量工具: timeit
'''
相对于 timeit 的细粒度，:mod:profile 和 pstats 模块提供了针对更大代码块的时间度量工具
'''
from timeit import Timer
t1 = Timer('t=a; a=b; b=t', 'a=1; b=2').timeit()
t2 = Timer('a,b = b,a', 'a=1; b=2').timeit()
print("{:} VS {:}".format(t1, t2))