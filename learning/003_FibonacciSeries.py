# 斐波纳契数列
## 两个元素的总和确定了下一个数
a, b = 0, 1
while b < 1000:
    print(b, end=", ")
    a, b = b, a + b
else:
    print("")
    print("结束啦~")
'''
可参考 ./005_IteratorAndGenerator.py 使用迭代器生成数列
'''