## Python3 模块
'''
从 Python 解释器退出再进入，那么之前定义的所有的方法和变量就都消失了。
为此 Python 提供了一个办法，把这些定义存放在文件中，为一些脚本或者交互式的解释器实例使用，这个文件被称为模块。

模块是一个包含所有你定义的函数和变量的文件，其后缀名是.py。
模块可以被别的程序引入，以使用该模块中的函数等功能。这也是使用 python 标准库的方法。
'''
import sys                                # 引入 python 标准库中的 sys.py 模块；这是引入某一模块的方法。

print("命令行参数如下：")
for arg in sys.argv:                      # sys.argv 是一个包含命令行参数的列表。
    print(arg)

print('\nPython 路径为：', sys.path, '\n') # sys.path 包含了一个 Python 解释器自动查找所需模块的路径的列表。

## import 语句
'''
想使用 Python 源文件，只需在另一个源文件里执行 import 语句，语法如下：
import module1[, module2[,... moduleN]
当解释器遇到 import 语句，如果模块在当前的搜索路径就会被导入。
一个模块只会被导入一次，不管你执行了多少次import。这样可以防止导入模块被一遍又一遍地执行。

搜索路径是一个解释器会先进行搜索的所有目录的列表（很像环境变量吧），被存储在 sys 模块中的 path 变量中。
搜索路径是在Python编译或安装的时候确定的，安装新的库应该也会修改。
'''
### 导入 fibo 模块
import pkg.sherlocky.fibo
'''
import 并没有把直接定义在 fibo 中的函数名称写入到当前符号表里，
只是把模块 fibo 的名字写到了那里。
'''
### 可以使用模块名称来访问函数：
pkg.sherlocky.fibo.fib(1000)
print(pkg.sherlocky.fibo.fib2(100))
f_fib = pkg.sherlocky.fibo.fib
f_fib(1000)
print(pkg.sherlocky.fibo.fib, f_fib, pkg.sherlocky.fibo.fib is f_fib, pkg.sherlocky.fibo.fib == f_fib)

## from … import 语句
'''
from 语句让我们可以从模块中导入一个指定的部分到当前命名空间中，语法如下：
from modname import name1[, name2[, ... nameN]]
'''
from pkg.sherlocky.fibo import fib
fib(1000)

## from … import * 语句
'''
把一个模块的所有内容全都导入到当前的命名空间也是可行的，只需使用如下声明：
from modname import *

这提供了一个简单的方法来导入一个模块中的所有项目。然而这种声明不该被过多地使用。

这将把所有的名字都导入进来，但是那些由单一下划线（_）开头的名字不在此例。
大多数情况， Python程序员不使用这种方法，因为引入的其它来源的命名，很可能覆盖了已有的定义。
'''

## 深入模块
'''
模块除了方法定义，还可以包括可执行的代码。这些代码一般用来初始化这个模块。这些代码只有在第一次被导入时才会被执行。

每个模块有各自独立的符号表，在模块内部为所有的函数当作全局符号表来使用。

所以，模块的作者可以放心大胆的在模块内部使用这些全局变量，而不用担心把其他用户的全局变量搞花。

从另一个方面，当你确实知道你在做什么的话，你也可以通过 modname.itemname 这样的表示法来访问模块内的函数。

模块是可以导入其他模块的。在一个模块（或者脚本，或者其他地方）的最前面使用 import 来导入一个模块，当然这只是一个惯例，而不是强制的。
被导入的模块的名称将被放入当前操作的模块的符号表中。
'''

## __name__属性
'''
一个模块被另一个程序第一次引入时，其主程序将运行。
如果我们想在模块被引入时，模块中的某一程序块不执行，我们可以用__name__属性来使该程序块仅在该模块自身运行时执行。
'''
import pkg.sherlocky.using_name
'''
说明： 每个模块都有一个__name__属性，当其值是'__main__'时，表明该模块自身在运行，否则是被引入。
说明：__name__ 与 __main__ 底下是双下划线， _ _ 是这样去掉中间的那个空格。
'''

## dir() 函数
'''
内置的函数 dir() 可以找到模块内定义的所有名称。以一个字符串列表的形式返回:
'''
print(dir(pkg.sherlocky.fibo))
'''
['__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'fib', 'fib2']
'''
print(dir(sys))
### 如果没有给定参数，那么 dir() 函数会罗列出当前模块定义的所有名称:
print(dir())
x = 5
print(dir())
del x
print(dir())

## 标准模块
'''
Python 本身带着一些标准的模块库，有些模块直接被构建在解析器里，这些虽然不是一些语言内置的功能，
但是他却能很高效的使用，甚至是系统级调用也没问题。
这些组件会根据不同的操作系统进行不同形式的配置，比如 winreg 这个模块就只会提供给 Windows 系统。
应该注意到这有一个特别的模块 sys ，它内置在每一个 Python 解析器中。
'''

## 包
'''
包是一种管理 Python 模块命名空间的形式，采用"点模块名称"。
比如一个模块的名称是 A.B， 那么他表示一个包 A中的子模块 B 。
就好像使用模块的时候，你不用担心不同模块之间的全局变量相互影响一样，采用点模块名称这种形式也不用担心不同库之间的模块重名的情况。
这样不同的作者都可以提供 NumPy 模块，或者是 Python 图形库。
'''
'''一种可能的包结构（在分层的文件系统中）:
sound/                          顶层包
      __init__.py               初始化 sound 包
      formats/                  文件格式转换子包
              __init__.py
              wavread.py
              wavwrite.py
              aiffread.py
              aiffwrite.py
              auread.py
              auwrite.py
              ...
'''
'''
在导入一个包的时候，Python 会根据 sys.path 中的目录来寻找这个包中包含的子目录。
目录只有包含一个叫做 __init__.py 的文件才会被认作是一个包，主要是为了避免一些滥俗的名字（比如叫做 string）不小心的影响搜索路径中的有效模块。

最简单的情况，放一个空的 __init__.py就可以了，这个文件中也可以包含一些初始化代码或者为__all__变量赋值。
用户可以每次只导入一个包里面的特定模块。
'''
'''
这将会导入子模块:sound.effects.echo。 他必须使用全名去访问:
sound.effects.echo.echofilter(input, output, delay=0.7, atten=4)

还有一种导入子模块的方法是:
from sound.effects import echo
这同样会导入子模块: echo，并且他不需要那些冗长的前缀，所以他可以这样使用:
echo.echofilter(input, output, delay=0.7, atten=4)

还有一种变化就是直接导入一个函数或者变量:
from sound.effects.echo import echofilter
同样的，这种方法会导入子模块: echo，并且可以直接使用他的 echofilter() 函数:
echofilter(input, output, delay=0.7, atten=4)

注意当使用from package import item这种形式的时候，对应的item既可以是包里面的子模块（子包），或者包里面定义的其他名称，比如函数，类或者变量。
import语法会首先把item当作一个包定义的名称，如果没找到，再试图按照一个模块去导入。如果还没找到，恭喜，一个:exc:ImportError 异常被抛出了。
反之，如果使用形如import item.subitem.subsubitem这种导入形式，除了最后一项，都必须是包，而最后一项则可以是模块或者是包，但是不可以是类，函数或者变量的名字。
'''

## 从一个包中导入*
'''导入语句遵循如下规则：
如果包定义文件 __init__.py 中存在一个叫做 __all__ 的列表变量，那么在使用 from package import * 的时候
就把这个列表中的所有名字作为包内容导入。

形如：
__all__ = ["echo", "surround", "reverse"]

作为包的作者，可别忘了在更新包之后保证 __all__ 也更新了！！！

如果 __all__ 真的没有定义，那么使用from package import *这种语法的时候，就不会导入包里的任何子模块。
他只是把包和它里面定义的所有内容导入进来（可能运行__init__.py里定义的初始化代码）。

这会把 __init__.py 里面定义的所有名字导入进来。并且他不会破坏掉我们在这句话之前导入的所有明确指定的模块。
'''
'''
记住，使用from Package import specific_submodule这种方法永远不会有错。
事实上，这也是【推荐】的方法。除非是你要导入的子模块有可能和其他包的子模块重名。
'''
'''
无论是隐式的还是显式的相对导入都是从当前模块开始的。
主模块的名字永远是"__main__"，一个Python应用程序的主模块，应当总是使用绝对路径引用。
'''
print("------------------------")
import pkg.sherlocky.hello          # 导入模块，必须使用全名去访问
pkg.sherlocky.hello.say("import 导入模块")

import pkg.sherlocky                # 导入子包，必须使用全名去访问
pkg.sherlocky.hello.say("import 导入子包")

from pkg.sherlocky import hello     # from 导入模块，可以直接访问
hello.say("from 导入模块")

from pkg.sherlocky.hello import say # from 导入模块函数，可以直接访问
say("from 导入模块函数")

from pkg.sherlocky.hello import *   # from 导入模块所有名字，可以直接访问
say2("from 导入模块中所有名字")

from pkg.sherlocky import *         # from import * 导入包下所有模块，包下 __init__.py 必须有 __all__
hi.sayHi("from import * 导入包下所有模块");

from pkg import *                   # from import * 导入所有子包，包下 __init__.py 必须有 __all__
print("from import * 导入所有子包")
print(sherlocky.fibo.fib2(200))