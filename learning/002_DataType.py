# 基本数据类型
'''
Python 中的变量不需要声明。每个变量在使用前都必须赋值，变量赋值以后该变量才会被创建。
在 Python 中，变量就是变量，它没有类型，我们所说的"类型"是变量所指的内存中对象的类型。
等号（=）用来给变量赋值。

一个变量可以通过赋值指向不同类型的对象
'''

## 多个变量赋值
a = b = c = 1
print(a, b, c)
a, b, c = 1, 2, "runoob"
print(a, b, c)

## 标准数据类型
'''
Number（数字）    -不可变数据
String（字符串）   -不可变数据
Tuple（元组）     -不可变数据
List（列表）      -可变数据
Set（集合）       -可变数据 
Dictionary（字典）-可变数据
'''
### isinstance 和 type 的区别在于：
'''
type() 不会认为子类是一种父类类型。
isinstance() 会认为子类是一种父类类型。
'''
print(isinstance(a, int));
print(type(a) == int);
### 可以通过使用 del 语句删除单个或多个对象。例如：
del a
del b, c
#print(a, b, c) # NameError: name 'a' is not defined

## 数值运算
print(5 + 4)  # 加法
print(4.3 - 2) # 减法
print(3 * 7)  # 乘法
print(2 / 4)  # 除法，得到一个浮点数  -- 0.5
print(2 // 4) # 除法，得到一个整数（整除） --0
print(17 % 3) # 取余    -- 2
print(2 ** 5) # 乘方    -- 32
###在混合计算时，Python会把整型转换成为浮点数。

## List（列表）: 列表是写在方括号 [] 之间、用逗号分隔开的元素列表。
'''
列表中元素的类型可以不相同，它支持数字，字符串甚至可以包含列表（所谓嵌套）。
和字符串一样，列表同样可以被索引和截取，列表被截取后返回一个包含所需元素的新列表。
截取的语法格式： 变量[头下标:尾下标]
索引值以 0 为开始值，-1 为从末尾的开始位置。
'''
list = [ 'abcd', 786 , 2.23, 'runoob', 70.2 ]
tinylist = [123, 'runoob']
print('------------------------------')
print (list)            # 输出完整列表
print (list[0])         # 输出列表第一个元素
print (list[1:3])       # 从第二个开始输出到第三个元素
print (list[2:])        # 输出从第三个元素开始的所有元素
print (tinylist * 2)    # 输出两次列表
print (list + tinylist) # 连接列表
### 与Python字符串不一样的是，列表中的元素是可以改变的：
a = [1, 2, 3, 4, 5, 6]
print(a)
a[0] = 9
print(a)
a[2:5] = [13, 14, 15]
print(a)
a[2:5] = []   # 将对应的元素值设置为 []
print(a)
print('------------------------------')
### 和字符串一样，列表截取也可以接收第三个参数，参数作用是截取的步长，如果第三个参数为负数表示逆向读取
list = [1, 2, 3, 4]
print(list)
# 第一个参数 -1 表示最后一个元素
# 第二个参数为空，表示移动到列表末尾
# 第三个参数为步长，-1 表示逆向
list = list[-1::-1] # 可以翻转列表
print(list)

## Tuple（元组）
'''
元组（tuple）与列表类似，不同之处在于元组的元素不能修改。
元组写在小括号 () 里，元素之间用逗号隔开。元组中的元素类型也可以不相同。
'''
print('------------------------------')
tuple = ( 'abcd', 786 , 2.23, 'runoob', 70.2  )
tinytuple = (123, 'runoob')
print (tuple)             # 输出完整元组
print (tuple[0])          # 输出元组的第一个元素
print (tuple[1:3])        # 输出从第二个元素开始到第三个元素
print (tuple[2:])         # 输出从第三个元素开始的所有元素
print (tinytuple * 2)     # 输出两次元组
print (tuple + tinytuple) # 连接元组
print('------------------------------')
### 其实，可以把字符串看作一种特殊的元组。
### 虽然tuple的元素不可改变，但它可以包含可变的对象，比如 list 列表。
tup1 = ()     # 空元组
tup2 = (20,)  # 一个元素，需要在元素后添加逗号
tup3 = (30)   # 一个元素时如果不加逗号 不会被认为是元组，此处会被当做 int 类型
tup4 = (40, 50)
print(tup1, tup2, tup3, tup4)
print(type(tup1), type(tup2), type(tup3), type(tup4))
'''
string、list 和 tuple 都属于 sequence（序列）。
'''

## Set（集合）
'''
集合（set）是由一个或数个形态各异的大小整体组成的，构成集合的事物或对象称作元素或是成员。
基本功能是进行成员关系测试和删除重复元素。
可以使用大括号 { } 或者 set() 函数创建集合，注意：创建一个空集合必须用 set() 而不是 { }，因为 { } 是用来创建一个空字典。
'''
print('------------------------------')
student = {'Tom', 'Jim', 'Mary', 'Tom', 'Jack', 'Rose'}
print(student)   # 输出集合，重复的元素被自动去掉
if 'Rose' in student :
    print("Rose 在集合中")
else :
    print('Rose 不在集合中')
### set可以进行集合运算
a = set('abracadabra')
b = set('alacazam')
print(a)
print(a - b)     # a 和 b 的差集
print(a | b)     # a 和 b 的并集
print(a & b)     # a 和 b 的交集
print(a ^ b)     # a 和 b 中不同时存在的元素

## Dictionary（字典）
'''
字典（dictionary）是Python中另一个非常有用的内置数据类型。
列表是有序的对象集合，字典是无序的对象集合。两者之间的区别在于：字典当中的元素是通过键来存取的，而不是通过偏移存取。
字典是一种映射类型，字典用 { } 标识，它是一个无序的 键(key) : 值(value) 的集合。
键(key)必须使用不可变类型。在同一个字典中，键(key)必须是唯一的。
'''
print('------------------------------')
_dict = {} # 创建空字典
_dict['one'] = "1 - 菜鸟教程"
_dict[2]     = "2 - 菜鸟工具"
tinydict = {'name': 'runoob','code':1, 'site': 'www.runoob.com'}
print (_dict['one'])       # 输出键为 'one' 的值
print (_dict[2])           # 输出键为 2 的值
print (tinydict)          # 输出完整的字典
print (tinydict.keys())   # 输出所有键
print (tinydict.values()) # 输出所有值
### 构造函数 dict() 可以直接从键值对序列中构建字典：
print('------------------------------')
print(dict([('Runoob', 1), ('Google', 2), ('Taobao', 3)])) # 可迭代对象方式来构造字典
print(dict(zip(['one', 'two', 'three'], [1, 2, 3])))   # 映射函数方式来构造字典
print({x: x**2 for x in (2, 4, 6)}) # {2: 4, 4: 16, 6: 36}
print(dict(Runoob=1, Google=2, Taobao=3)) # 传入关键字
print('------------------------------')