# 计算小狗的年龄

age = int(input("请输入小狗的年龄："))
print("")
if age < 0:
    print("你是在逗我吗？")
else:
    if age == 1:
        print('相当于14岁的人')
    elif age == 2:
        print('相当于22岁的人')
    elif age > 2:
        print("对应人类年龄: ", 22 + (age - 2) * 5)

### 退出提示
input("点击任意键退出程序~")