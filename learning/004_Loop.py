# 循环语句
## while循环（可以使用 else 语句）
n, sum = 100, 0
counter = 1
while counter <= n:
    sum += counter
    counter += 1 # 没有 n++ 这种写法
else:
    print("1 到 %d 之和为: %d" % (n, sum))
### 无线循环
### 类似if语句的语法，如果while循环体中只有一条语句，可以将该语句与while写在同一行中
flag = 0 # 1
while (flag): print ('Welcome!')
print ("Good bye!")
print('------------------------------')

## for 循环（也可以使用 else 语句）
languages = ["C", "C++", "Java", "Perl", "Python"]
for x in languages:
    if x == 'Java':
        print("Java~~")
        break
    print("循环数据：" + x)
else:
    print("没有循环数据!")
print("完成循环~")
print('------------------------------')

## range() 函数
for i in range(8):
    print(i, end=", ")
print("")
for i in range(1, 5):
    print(i, end=", ")
print("")
### 也可以使range以指定负数开始并指定不同的步长
for i in range(-10, -100, -30) :
    print(i, end=", ")
print("")
### 和 len() 结合
a = ['Google', 'Baidu', 'Runoob', 'Taobao', 'QQ']
for i in range(len(a)):
    print(i, a[i])
### 使用range()函数来创建一个列表
print(list(range(5)))
print('------------------------------')

## 也可以使用 break 和 continue 语句

## pass 语句：是空语句，是为了保持程序结构的完整性。
### pass 不做任何事情，一般用做占位语句
for letter in 'Runoob':
    if letter == 'o':
        pass
        print ('执行 pass 块')
    print ('当前字母 :', letter)
print ("Good bye!")